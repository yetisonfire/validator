package com.meritgp.validator.constant;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Holidays {


    public static final String holidaysUSD = "2016-01-02,2016-01-16,2016-02-20,2016-04-14,2016-04-17,2016-05-01,2016-05-25,2016-05-29,2016-06-05,2016-07-04,2016-08-01,2016-09-04,2016-10-09,2016-11-23,2016-12-25,2016-12-26";
    public static final String holidaysEUR = "2016-01-02,2016-04-14,2016-04-17,2016-05-01,2016-05-25,2016-06-05,2016-08-01,2016-12-25,2016-12-26";

    public static List<LocalDate> genData(String data) {

        List<LocalDate> dateList = new LinkedList<>();

        String[] datesStr = data.split(",");

        for (String str : datesStr) {
            dateList.add(LocalDate.parse(str));
        }

        return dateList;

    }


}
