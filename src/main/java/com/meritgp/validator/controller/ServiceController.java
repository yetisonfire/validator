package com.meritgp.validator.controller;


import com.google.gson.Gson;
import com.meritgp.validator.model.*;
import com.meritgp.validator.validator.ForwardValidator;
import com.meritgp.validator.validator.SpotValidator;
import com.meritgp.validator.validator.VanillaOptionValidator;
import com.meritgp.validator.validator.enitites.ValidatorResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


/**
 * Main part of the application
 * Controller which serve data for validation,
 * and than return data with specified errors
 */
@RestController
public class ServiceController {

    private Logger logger = LoggerFactory.getLogger(ServiceController.class);


    /**
     * At 1 step - consumes any data as a String, transform it into JsonModel
     * At 2 step - transform JsonModel into financial derivatives {@link Spot}, {@Link Forward}, {@Link VanillaOption}
     * and validate according derivatives requirement
     *
     * @param data - any data that fit JSON format {test:[{...},{...},{...}... n]}
     * @return  JSON data in format [{"validatedObject":{object...},"errors":[{error...}] }]
     */
    @RequestMapping(value = "/data", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ValidatorResult[] validator(@RequestParam String data) {


        //step 1 receive and parse
        Gson gson = new Gson();
        JsonModel[] jsonModels = null;

        ValidatorResult[] results = null;
        try {
            jsonModels = gson.fromJson(data, ArrayOfModels.class).getTest();


            int jsonModelsLength = jsonModels.length;

            results = new ValidatorResult[jsonModelsLength];  //create array of results

            SpotValidator spotValidator = new SpotValidator();
            ForwardValidator forwardValidator = new ForwardValidator();
            VanillaOptionValidator vanillaOptionValidator = new VanillaOptionValidator();

            //step 2 validate objects
            for (int counter = 0; counter < jsonModelsLength; counter++) {
                switch (jsonModels[counter].getType()) {
                    case "Spot": {
                        results[counter] = spotValidator.validate(new Spot(jsonModels[counter]));
                        break;
                    }
                    case "Forward": {
                        results[counter] = forwardValidator.validate(new Forward(jsonModels[counter]));
                        break;
                    }
                    case "VanillaOption": {
                        results[counter] = vanillaOptionValidator.validate(new VanillaOption(jsonModels[counter]));
                        break;
                    }
                    default: {
                        logger.error("step 2: wrong type of jsonModel" + counter);
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Incoming data error " + e);
        }
        return results;


    }


}
