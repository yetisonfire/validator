package com.meritgp.validator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Simple shutdown for application
 */
@Controller
public class ShutdownController {


    /**
     *   url mapper for shutdown operation
     */
    @RequestMapping(value = "/shutdown", method = RequestMethod.POST)
    public void shutdown(){
        System.exit(0);
    }


}
