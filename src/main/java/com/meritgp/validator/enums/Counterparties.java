package com.meritgp.validator.enums;


/**
 * Constant values for counter Counterparties(consumers)
 * using to validate incoming data
 */
public enum Counterparties {
    PLUTO1, PLUTO2;
}
