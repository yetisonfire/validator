package com.meritgp.validator.enums;


/**
 * Pair constant when T+1 else T+2
 */
public enum ForwardAndSpotCcyPair {

    USDCAD, USDTRY, USDPHP, USDRUB;
}
