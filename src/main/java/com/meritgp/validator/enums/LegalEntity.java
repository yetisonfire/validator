package com.meritgp.validator.enums;


/**
 * @since 0.0.1
 * Only one possible value for Legal Entity is "CS Zurich"
 */
public enum LegalEntity {

    CS_ZURICH("CS Zurich");

    private String val;


    /**
     * Constructor to assign constant value
     * @param str - constant value
     */
    LegalEntity(String str) {
        val = str;
    }



    //getter and setter
    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
