package com.meritgp.validator.enums;


/**
 *  Constant of two possible styles of VanillaOption
 */
public enum Style {
    American, European;
}
