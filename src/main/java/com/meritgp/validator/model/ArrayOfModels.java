package com.meritgp.validator.model;


/**
 * Special container for receiving test data that fit JSON format {test:[{...},{...},{...}... n]}
 *
 */
public class ArrayOfModels {

    /**
     * Array of all data in one common view/format JsonModel
     */
    private JsonModel[] test;



    //getter and setter
    public JsonModel[] getTest() {
        return test;
    }

    public void setTest(JsonModel[] test) {
        this.test = test;
    }
}
