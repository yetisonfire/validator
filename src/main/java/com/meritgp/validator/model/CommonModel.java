package com.meritgp.validator.model;

import java.math.BigDecimal;

/**
 * @since 0.0.1 - Superclass for all financial derivatives
 */
public abstract class CommonModel {
    /**
     * Common fields that exists in Forward, Spot, VanillaOption
     */
    private String customer;

    /**
     * Pair of currencies
     **/
    private String ccyPair;

    private String type;

    private String direction;

    private String tradeDate;

    private BigDecimal rate;

    /**
     * {@link com.meritgp.validator.enums.LegalEntity}
     **/
    private String legalEntity;

    private BigDecimal amount1;

    private BigDecimal amount2;
    
    private String trader;

    //getters and setters
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public BigDecimal getAmount1() {
        return amount1;
    }

    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }

    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }


    @Override
    public String toString() {
        return
                "customer='" + customer + '\'' +
                        ", ccyPair='" + ccyPair + '\'' +
                        ", type='" + type + '\'' +
                        ", direction='" + direction + '\'' +
                        ", tradeDate='" + tradeDate + '\'' +
                        ", rate=" + rate +
                        ", legalEntity='" + legalEntity + '\'' +
                        ", amount1=" + amount1 +
                        ", amount2=" + amount2 +
                        ", trader='" + trader + '\'';
    }
}
