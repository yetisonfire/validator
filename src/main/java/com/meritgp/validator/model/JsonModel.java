package com.meritgp.validator.model;

import java.math.BigDecimal;


/**
 * All data that received from client,
 * some fields could be equal to null
 */

public class JsonModel {
    //Start of CommonModel fields
    private String customer;

    private String ccyPair;

    private String type;

    private String direction;

    private String tradeDate;

    private BigDecimal rate;

    private String legalEntity;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private String trader;
    //End of CommonModel fields

    //Start of spot and forward fields
    /**
     * @since 0.0.1 - Belongs only to Forward and Spot
     */
    private String valueDate;
    //End of spot and forward fields


    /**
     * @since 0.0.1 - Belongs only to VanillaOption
     */
    //Start of vanillaOption fields
    private String style;

    private String strategy;

    private String deliveryDate;

    private String expiryDate;

    private String payCcy;

    private BigDecimal premium;

    private String excerciseStartDate;

    private String premiumCcy;

    private String premiumType;

    private String premiumDate;
    // End of vanillaOption fields


    //getters and setters
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public BigDecimal getAmount1() {
        return amount1;
    }

    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }

    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPayCcy() {
        return payCcy;
    }

    public void setPayCcy(String payCcy) {
        this.payCcy = payCcy;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public String getExcerciseStartDate() {
        return excerciseStartDate;
    }

    public void setExcerciseStartDate(String excerciseStartDate) {
        this.excerciseStartDate = excerciseStartDate;
    }

    public String getPremiumCcy() {
        return premiumCcy;
    }

    public void setPremiumCcy(String premiumCcy) {
        this.premiumCcy = premiumCcy;
    }

    public String getPremiumType() {
        return premiumType;
    }

    public void setPremiumType(String premiumType) {
        this.premiumType = premiumType;
    }

    public String getPremiumDate() {
        return premiumDate;
    }

    public void setPremiumDate(String premiumDate) {
        this.premiumDate = premiumDate;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("JsonModel{");
        sb.append("customer='").append(customer).append('\'');
        sb.append(", ccyPair='").append(ccyPair).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", direction='").append(direction).append('\'');
        sb.append(", tradeDate='").append(tradeDate).append('\'');
        sb.append(", rate=").append(rate);
        sb.append(", legalEntity='").append(legalEntity).append('\'');
        sb.append(", amount1=").append(amount1);
        sb.append(", amount2=").append(amount2);
        sb.append(", trader='").append(trader).append('\'');
        sb.append(", valueDate='").append(valueDate).append('\'');
        sb.append(", style='").append(style).append('\'');
        sb.append(", strategy='").append(strategy).append('\'');
        sb.append(", deliveryDate='").append(deliveryDate).append('\'');
        sb.append(", expiryDate='").append(expiryDate).append('\'');
        sb.append(", payCcy='").append(payCcy).append('\'');
        sb.append(", premium=").append(premium);
        sb.append(", excerciseStartDate='").append(excerciseStartDate).append('\'');
        sb.append(", premiumCcy='").append(premiumCcy).append('\'');
        sb.append(", premiumType='").append(premiumType).append('\'');
        sb.append(", premiumDate='").append(premiumDate).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

