package com.meritgp.validator.model;


/**
 *  Financial derivative
 *  @since 0.0.1 - one feature is valueDate
 *  extends CommonModel to achieve fields, that common, for all fin derivatives
 *  Same as Forward, but easy to extends in future
 */
public class Spot extends CommonModel {

    /**
     *  A value date is a future date used in determining the value of a product that fluctuates in price.
     *  Typically, you will see the use of value dates in determining the payment of products and accounts
     *  where there is a possibility for discrepancies due to differences in the timing of valuation.
     *  Such products include forward currency contracts, option contracts, and the interest payable or receivable on personal accounts.
     *  from http://www.investopedia.com/terms/v/valuedate.asp
     *
     */
    private String valueDate;


    //getter and setter
    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    /**
     * Constructor
     * In case of manual field initialization or some other constructor methods
     */
    public Spot() {

    }


    /**
     * Special constructor to copy data from JsonModel and create fully qualified Forward object
     * using fields from superclass  CommonModel
     * @param jsonModel - {@link JsonModel}
     */
    public Spot(JsonModel jsonModel) {
        this.setCustomer(jsonModel.getCustomer());
        this.setCcyPair(jsonModel.getCcyPair());
        this.setType(jsonModel.getType());
        this.setDirection(jsonModel.getDirection());
        this.setTradeDate(jsonModel.getTradeDate());
        this.setRate(jsonModel.getRate());
        this.setLegalEntity(jsonModel.getLegalEntity());
        this.setAmount1(jsonModel.getAmount1());
        this.setAmount2(jsonModel.getAmount2());
        this.setTrader(jsonModel.getTrader());
        this.setValueDate(jsonModel.getValueDate());
    }


    @Override
    public String toString() {
        return "Spot{" +
                "valueDate='" + valueDate + '\''
                + super.toString() + "} ";
    }
}
