package com.meritgp.validator.model;

import java.math.BigDecimal;


/**
 * Financial derivative
 *
 * @since 0.0.1 - have several features
 * extends CommonModel to achieve fields, that common, for all fin derivatives
 */
public class VanillaOption extends CommonModel {

    /**
     * {@link com.meritgp.validator.enums.Style}
     **/
    private String style;

    private String strategy;

    private String deliveryDate;

    private String expiryDate;

    private String payCcy;

    private BigDecimal premium;

    /*  Optional
     *  exist only if style equals AMERICAN
     */
    private String excerciseStartDate;

    private String premiumCcy;

    private String premiumType;

    private String premiumDate;


    /**
     * Constructor to copy data from JsonModel
     * {@link JsonModel}
     *
     * @param jsonModel
     */
    public VanillaOption(JsonModel jsonModel) {
        //common fields
        this.setCustomer(jsonModel.getCustomer());
        this.setCcyPair(jsonModel.getCcyPair());
        this.setType(jsonModel.getType());
        this.setDirection(jsonModel.getDirection());
        this.setTradeDate(jsonModel.getTradeDate());
        this.setRate(jsonModel.getRate());
        this.setLegalEntity(jsonModel.getLegalEntity());
        this.setAmount1(jsonModel.getAmount1());
        this.setAmount2(jsonModel.getAmount2());
        this.setTrader(jsonModel.getTrader());

        //vanlla option fields
        this.setStyle(jsonModel.getStyle());
        this.setStrategy(jsonModel.getStrategy());
        this.setDeliveryDate(jsonModel.getDeliveryDate());
        this.setExpiryDate(jsonModel.getExpiryDate());
        this.setPayCcy(jsonModel.getPayCcy());
        this.setPremium(jsonModel.getPremium());
        this.setExcerciseStartDate(jsonModel.getExcerciseStartDate());
        this.setPayCcy(jsonModel.getPremiumCcy());
        this.setPremiumCcy(jsonModel.getPremiumCcy());
        this.setPremiumType(jsonModel.getPremiumType());
        this.setPremiumDate(jsonModel.getPremiumDate());
    }


    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPayCcy() {
        return payCcy;
    }

    public void setPayCcy(String payCcy) {
        this.payCcy = payCcy;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public String getExcerciseStartDate() {
        return excerciseStartDate;
    }

    public void setExcerciseStartDate(String excerciseStartDate) {
        this.excerciseStartDate = excerciseStartDate;
    }

    public String getPremiumCcy() {
        return premiumCcy;
    }

    public void setPremiumCcy(String premiumCcy) {
        this.premiumCcy = premiumCcy;
    }

    public String getPremiumType() {
        return premiumType;
    }

    public void setPremiumType(String premiumType) {
        this.premiumType = premiumType;
    }

    public String getPremiumDate() {
        return premiumDate;
    }

    public void setPremiumDate(String premiumDate) {
        this.premiumDate = premiumDate;
    }
}
