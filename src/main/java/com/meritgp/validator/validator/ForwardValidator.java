package com.meritgp.validator.validator;

import com.meritgp.validator.model.Forward;
import com.meritgp.validator.validator.enitites.ValidatorResult;

import java.time.LocalDate;


/**
 * ForwardsValidator implementation
 */
public final class ForwardValidator extends Validator<Forward> {


    @Override
    public ValidatorResult validate(Forward model) {
        this.result = new ValidatorResult(model);

        // because of multiple using
        final LocalDate valueDate = LocalDate.parse(model.getValueDate());
        final LocalDate tradeDate = LocalDate.parse(model.getTradeDate());

        final String currencyVal1 = model.getCcyPair().substring(0, 3);
        final String currencyVal2 = model.getCcyPair().substring(3);

        counterpartiesValidation(model.getCustomer());

        legalEntityValidation(model.getLegalEntity());

        currencyISOValidation(currencyVal1, currencyVal2);

        dateValidation(valueDate, tradeDate);

        holidaysValidator(valueDate, currencyVal1, currencyVal2);



        return result;
    }



}
