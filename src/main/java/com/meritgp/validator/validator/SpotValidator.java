package com.meritgp.validator.validator;

import com.meritgp.validator.model.Spot;
import com.meritgp.validator.validator.enitites.ValidatorResult;

import java.time.LocalDate;


/**
 * SpotValidator implementation
 */
public final class SpotValidator extends Validator<Spot> {


    @Override
    public ValidatorResult validate(Spot model) {
        this.result = new ValidatorResult(model);

        // because of multiple using
        final LocalDate valueDate = LocalDate.parse(model.getValueDate());
        final LocalDate tradeDate = LocalDate.parse(model.getTradeDate());

        final String currencyVal1 = model.getCcyPair().substring(0, 3);
        final String currencyVal2 = model.getCcyPair().substring(3);


        legalEntityValidation(model.getLegalEntity());

        counterpartiesValidation(model.getCustomer());

        currencyISOValidation(currencyVal1, currencyVal2);

        dateValidation(valueDate, tradeDate);

        holidaysValidator(valueDate, currencyVal1, currencyVal2);


        return result;
    }


}
