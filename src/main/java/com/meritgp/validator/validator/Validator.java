package com.meritgp.validator.validator;

import com.meritgp.validator.constant.Holidays;
import com.meritgp.validator.enums.Counterparties;
import com.meritgp.validator.enums.ForwardAndSpotCcyPair;
import com.meritgp.validator.enums.ISO4217;
import com.meritgp.validator.enums.LegalEntity;
import com.meritgp.validator.model.CommonModel;
import com.meritgp.validator.validator.enitites.ValidatorResult;

import java.io.InputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * Class with common methods for classes that extends CommonModel (Forward, Spot, VanillaOption)
 *
 * @param <T> - any class that extend CommonModel
 *            Inspired by Hibernate-validator
 */
public abstract class Validator<T extends CommonModel> {

    /**
     * Container for validation object and its errors
     * !IMPORTANT! Should be initialized in subclasses
     */
    ValidatorResult result = null;


    /**
     * Validation starts here
     * Within should be invoked any methods that lay below
     *
     * @param model - object to validate, Any object that extends CommonModel
     * @return - Container for validation object and its errors
     */
    abstract ValidatorResult validate(T model);

    /**
     * Check models currencies for according to ISO4217
     *
     * @param currency1 - first val from ccyPair
     * @param currency2 - second val from ccyPair
     */
    protected void currencyISOValidation(String currency1, String currency2) {

        int cur1Valid = 0;
        int cur2Valid = 0;

        for (ISO4217 iso : ISO4217.values()) {
            if (iso.toString().equals(currency1)) {
                cur1Valid++;
            }
            if (iso.toString().equals(currency2)) {
                cur2Valid++;
            }
        }
        if (cur1Valid == 0) {
            result.commitError("CcyPair", "Currency 1 failed ISO4217 validation, actual: " + currency1);
        }
        if (cur2Valid == 0) {
            result.commitError("CcyPair", "Currency 2 failed ISO4217 validation, actual: " + currency2);

        }
    }


    /**
     * Check models for according to counterparties from condition -  current (PLUTO1, PLUTO2)
     *
     * @param customer - value of model customer
     */
    protected void counterpartiesValidation(String customer) {
        int counterparties = 0;
        for (Counterparties cont : Counterparties.values()) {
            if (cont.toString().equals(customer)) {
                counterparties++;
            }
        }
        if (counterparties == 0) {
            result.commitError("customer", "counterparty(customer) is not supported, actual: " + customer);
        }
    }


    /**
     * Check models for according to one Legal Entity "CS ZURICH"
     *
     * @param legalEntity - value of model legalEntity
     */
    protected void legalEntityValidation(String legalEntity) {
        if (!legalEntity.equals(LegalEntity.CS_ZURICH.getVal())) {
            result.commitError("legalEntity", "Field legalEntity should be equal CS Zurich ");
        }
    }


    /**
     * Compare valueDate with holidays
     * eur.csv and usd.csv taken from FX calendar on https://www.six-swiss-exchange.com
     */
    protected void holidaysValidator(LocalDate valueDate, String currencyVal1, String currencyVal2) {


        try (InputStream is = getClass().getResourceAsStream("/holidays/" + currencyVal1.toLowerCase() + ".csv")) {
            Scanner scanner = new Scanner(is);
            while (scanner.hasNextLine()) {
                LocalDate date = LocalDate.parse(scanner.nextLine());

                if (date.isEqual(valueDate)) {
                    result.commitError("valueDate", "valueDate can not be in currency1 non-working day, actual: " + date);
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        try (InputStream is = getClass().getResourceAsStream("/holidays/" + currencyVal2.toLowerCase() + ".csv")) {
            Scanner scanner = new Scanner(is);
            while (scanner.hasNextLine()) {
                LocalDate date = LocalDate.parse(scanner.nextLine());

                if (date.isEqual(valueDate)) {

                    result.commitError("valueDate", "valueDate can not be in currency2 non-working day, actual: " + date);

                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * @param valueDate
     * @param tradeDate
     * @since 0.0.1 available only for Forward and Spot
     */
    protected void dateValidation(LocalDate valueDate, LocalDate tradeDate) {
        if (valueDate.isBefore(tradeDate)) {
            result.commitError("valueDate", "value date cannot be before trade date, actual: " + valueDate + " " + tradeDate);
        }
        if (valueDate.getDayOfWeek() == DayOfWeek.SUNDAY || valueDate.getDayOfWeek() == DayOfWeek.SATURDAY) {
            result.commitError("valueDate", "value date cannot fall on weekend or non-working day for currency, actual: " + valueDate.getDayOfWeek());
        }

    }



}
