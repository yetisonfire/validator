package com.meritgp.validator.validator;

import com.meritgp.validator.enums.Style;
import com.meritgp.validator.model.VanillaOption;
import com.meritgp.validator.validator.enitites.ValidatorResult;

import java.time.LocalDate;

/**
 * VanillaOptionValidator implementation
 */
public class VanillaOptionValidator extends Validator<VanillaOption> {


    @Override
    public ValidatorResult validate(VanillaOption model) {
        this.result = new ValidatorResult(model);


        final String currencyVal1 = model.getCcyPair().substring(0, 3);
        final String currencyVal2 = model.getCcyPair().substring(3);

        styleValidation(model);
        dateValidation(model);
        legalEntityValidation(model.getLegalEntity());
        counterpartiesValidation(model.getCustomer());

        currencyISOValidation(currencyVal1, currencyVal2);
        return result;
    }

    /**
     * VanillaOption style validation according to tech requirement
     * * @param model
     * Style can be either American or European
     */
    private void styleValidation(VanillaOption model) {
        int styleValid = 0;
        for (Style style : Style.values()) {
            if (style.toString().equalsIgnoreCase(model.getStyle())) {
                styleValid++;
            }
        }

        if (styleValid == 0) {
            result.commitError("Style", "Style can be either American or European, actual: " + model.getStyle());
        }
    }

    /**
     * VanillaOption date validation according to tech requirement
     * @param model - object of VanillaOption
     * several date conditions
     */
    private void dateValidation(VanillaOption model) {
        LocalDate tradeDate = LocalDate.parse(model.getTradeDate());
        LocalDate expiryDate = LocalDate.parse(model.getExpiryDate());
        LocalDate premiumDate = LocalDate.parse(model.getPremiumDate());
        LocalDate deliveryDate = LocalDate.parse(model.getDeliveryDate());

        if (model.getStyle().equalsIgnoreCase(Style.American.toString())) {
            LocalDate excerciseStartDate = LocalDate.parse(model.getExcerciseStartDate());

            if (!excerciseStartDate.isAfter(tradeDate)) {
                result.commitError("excercideStartDate", "excercideStartDate should be after tradeDate, actual: " + excerciseStartDate + " " + tradeDate);
            }

            if (!excerciseStartDate.isBefore(expiryDate)) {
                result.commitError("excercideStartDate", "excercideStartDate should be before expiryDate, actual: " + excerciseStartDate + " " + expiryDate);
            }
        }

        if (!expiryDate.isBefore(deliveryDate)) {
            result.commitError("deliveryDate", "expiryDate should be before delivery date, actual: " + expiryDate + " " + deliveryDate);
        }
        if (!premiumDate.isBefore(deliveryDate)) {
            result.commitError("deliveryDate", "premiumDate should be before delivery date, actual: " + expiryDate + " " + premiumDate);

        }
    }
}
