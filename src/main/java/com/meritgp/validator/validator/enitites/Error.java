package com.meritgp.validator.validator.enitites;

/**
 * Container for field and reason of error
 * for several errors in one field will be several Error objects
 */
public class Error {
    /**
     * name of the field where found an error
     */
    private String field;
    /**
     *
     */
    private String reason;

    /**
     * Constructor for easy creation of Error in commitError method of {@link ValidatorResult}
     *
     * @param field
     * @param reason
     */
    public Error(String field, String reason) {
        this.field = field;
        this.reason = reason;
    }


    //getters and setters
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Error{" +
                "field='" + field + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
