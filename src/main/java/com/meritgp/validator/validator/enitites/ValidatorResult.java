package com.meritgp.validator.validator.enitites;

import java.util.LinkedList;
import java.util.List;

public class ValidatorResult<T> {

    //fields
    private T validatedObject;

    private List<Error> errors;


    //constructor
    public ValidatorResult(T model) {
        this.validatedObject = model;
        errors = new LinkedList<Error>();
    }

    /**
     * @params(fied, reason) to create Error
     */
    public void commitError(String field, String reason) {
        errors.add(new Error(field, reason));
    }


    //getters and setters
    public T getValidatedObject() {
        return validatedObject;
    }

    public void setValidatedObject(T validatedObject) {
        this.validatedObject = validatedObject;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ValidatorResult{" +
                "validatedObject=" + validatedObject +
                ", errors=" + errors +
                '}';
    }
}
