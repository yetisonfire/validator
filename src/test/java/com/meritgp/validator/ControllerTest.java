package com.meritgp.validator;


import com.meritgp.validator.controller.ServiceController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ServiceController.class)

public class ControllerTest {

    private Logger logger = LoggerFactory.getLogger(ControllerTest.class);


    /**
     * Short variant of test data
     */
    private String testData =
            "    {test:\n" +
                    "    [\n" +
                    "        {\"customer\":\"PLUTO1\",\"ccyPair\":\"EURUSD\",\"type\":\"Spot\",\"direction\":\"BUY\",\"tradeDate\":\"2016-08-11\",\"amount1\":1000000.00,\"amount2\":1120000.00,\"rate\":1.12,\"valueDate\":\"2016-08-15\",\"legalEntity\":\"CS Zurich\",\"trader\":\"JohannBaumfiddler\"}]}\n";

    @Autowired
    private MockMvc mockMvc;

    /**
     * Check main page for 200 status
     *
     * @throws Exception in case perform(...) method will not connect to page
     */
    @Test
    public void testMainPage() throws Exception {
        logger.info("TEST STARTED");
        this.mockMvc.perform(get("/index.html").accept(MediaType.ALL))
                .andExpect(status().isOk());
        logger.info("TEST FINISHED");
    }


    /**
     * Check service for 200 status
     *
     * @throws Exception
     */
    @Test
    public void testService() throws Exception {
        logger.info("TEST STARTED");
        this.mockMvc.perform(post("/data").param("data", testData)).andExpect(status().isOk());
        logger.info("TEST FINISHED");

    }


}
