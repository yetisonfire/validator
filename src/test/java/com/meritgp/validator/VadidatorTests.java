package com.meritgp.validator;

import com.google.gson.Gson;
import com.meritgp.validator.model.*;
import com.meritgp.validator.validator.ForwardValidator;
import com.meritgp.validator.validator.VanillaOptionValidator;
import com.meritgp.validator.validator.enitites.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@Test
public class VadidatorTests {
    Logger logger = LoggerFactory.getLogger(VadidatorTests.class);
    JsonModel[] model = null;
    Gson gson;

    /**
     * @throws Exception - can throw FileNotFoundException if the path is broken
     */
    @BeforeTest
    public void readAndAssignJson() throws Exception {
        gson = new Gson();
        model = gson.fromJson(new FileReader("/home/lynx86/IdeaProjects/validator/src/test/resourses/testData.txt"), ArrayOfModels.class).getTest();
        logger.info("DATA LOADED");
    }


    /**
     * Check multiple databinding from file
     */
    @Test
    public void checkDataBinding() {
        logger.info("TEST STARTED");
        for (int counter = 0; counter < model.length; counter++) {
            switch (model[counter].getType()) {
                case "Spot": {
                    Spot spot = new Spot(model[counter]);
                    logger.info("success:" + spot);
                    break;
                }
                case "Forward": {
                    Forward forward = new Forward(model[counter]);
                    logger.info("success:" + forward);
                    break;
                }
                case "VanillaOption": {
                    VanillaOption vanillaOption = new VanillaOption(model[counter]);
                    logger.info("success:" + vanillaOption);
                    break;
                }
                default: {
                    logger.error("binding failed on" + counter);
                }
            }
        }
        logger.info("TEST FINISHED");
    }

    /**
     * Purposely break the data on customer -> cusstomer
     */
    @Test
    public void databindFail() {
        logger.info("TEST STARTED");
        JsonModel model = gson.fromJson("{\"cusstomer\":\"PLUTO3\",\"ccyPair\":\"EURUSD\",\"type\":\"VanillaOption\",\"style\":\"AMERICAN\",\"direction\":\"SELL\",\"strategy\":\"CALL\",\"tradeDate\":\"2016-08-11\",\"amount1\":1000000.00,\"amount2\":1120000.00,\"rate\":1.12,\"deliveryDate\":\"2016-08-22\",\"expiryDate\":\"2016-08-19\",\"excerciseStartDate\":\"2016-08-10\",\"payCcy\":\"USD\",\"premium\":0.20,\"premiumCcy\":\"USD\",\"premiumType\":\"%USD\",\"premiumDate\":\"2016-08-12\",\"legalEntity\":\"CS Zurich\",\"trader\":\"Johann Baumfiddler\"}\n", JsonModel.class);
        VanillaOption vp = new VanillaOption(model);
        logger.info(vp.toString());
        Assert.assertTrue(vp.getCustomer() == null);
        logger.info("TEST FINISHED");

    }

    /**
     * Purposely made 5 mistakes
     * 1 excercideStartDate before tradeDate
     * 2 expiryDate after deliveryDate
     * 3 premiumDate after deliveryDate
     * 4 not supported counterparty  - PLUTO3
     * 5 currency not valid ISO4217
     */
    @Test
    public void vainllaOptionValidation() {
        logger.info("TEST STARTED");
        VanillaOptionValidator validator = new VanillaOptionValidator();
        JsonModel model = gson.fromJson("{\"customer\":\"PLUTO3\",\"ccyPair\":\"OURUSD\",\"type\":\"VanillaOption\",\"style\":\"AMERICAN\",\"direction\":\"SELL\",\"strategy\":\"CALL\",\"tradeDate\":\"2016-08-11\",\"amount1\":1000000.00,\"amount2\":1120000.00,\"rate\":1.12,\"deliveryDate\":\"2016-08-01\",\"expiryDate\":\"2016-08-19\",\"excerciseStartDate\":\"2016-08-09\",\"payCcy\":\"USD\",\"premium\":0.20,\"premiumCcy\":\"USD\",\"premiumType\":\"%USD\",\"premiumDate\":\"2016-08-12\",\"legalEntity\":\"CS Zurich\",\"trader\":\"Johann Baumfiddler\"}\n", JsonModel.class);
        List<Error> errors = validator.validate(new VanillaOption(model)).getErrors();
        for (Error e : errors) {
            logger.info("" + e);
        }
        logger.info("Errors in data " + errors.size());
        Assert.assertEquals(errors.size(), 5);

        logger.info("TEST FINISHED");

    }


    /**
     * Purposely made 3 mistakes:
     * 1 valueDate before tradeDate
     * 2 valueDate is holiday for usd
     * 3 valueDate is a holiday for eur
     */
    @Test
    public void forwardDateValidation() throws FileNotFoundException {
        logger.info("TEST STARTED");
        ForwardValidator validator = new ForwardValidator();
        JsonModel model = gson.fromJson("{\"customer\":\"PLUTO2\",\"ccyPair\":\"EURUSD\",\"type\":\"Forward\",\"direction\":\"SELL\",\"tradeDate\":\"2017-09-24\",\"amount1\":1000000.00,\"amount2\":1120000.00,\"rate\":1.12,\"valueDate\":\"2016-04-14\",\"legalEntity\":\"CS Zurich\",\"trader\":\"JohannBaumfiddler\"}", JsonModel.class);

        List<Error> errors = validator.validate(new Forward(model)).getErrors();
        logger.info("Errors in data " + errors.size());
        Assert.assertEquals(errors.size(), 3);
        logger.info("TEST FINISHED");
    }



}
